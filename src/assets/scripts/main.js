window.onload = (e) => {
  let scrollLine = document.querySelector('.scroll-line')
  let scroll = document.querySelector('.scroll')
  let carouselTrack = document.querySelector('.carousel-track')
  let isDrag = false //нажат ли элемент
  let positionMouseX  //координата мыши
  let offsetX = 0; //запомнили позицию scroll при нажатиии
  let positionTrackX = 0; //запомнили позицию carouselTrack при нажатиии
  let scrollLinePosLeft = scrollLine.offsetLeft
  let scrollLinePosRight = scrollLine.getBoundingClientRect().width
  let carouselTrackWidth = carouselTrack.getBoundingClientRect().width
  console.log('carouselTrackWidth', carouselTrackWidth)
  scroll.onmousedown = function (e) {
    isDrag = true
    positionMouseX = e.pageX;
    offsetX = scroll.offsetLeft
  }

  window.addEventListener('mousemove', e => {
    if (isDrag === true) {
      let offset = e.pageX - positionMouseX
      // console.log('scroll.offsetLeft', scroll.offsetLeft)
      // console.log('scrollLinePosRight', scrollLinePosRight)
      // console.log('scrollLinePosLeft', scrollLinePosLeft)
      console.log('offset', offset)
      let test = carouselTrackWidth / scrollLine.getBoundingClientRect().width
      carouselTrack.style = `left: -${offset * (test/ 2)}px`
      scroll.style = `left: ${offsetX + offset}px`
      if(scroll.offsetLeft < 0){
        // isDrag = false
        console.log('вышел за границу влево')
        scroll.style = `left: ${0}px`
      }
      if((scroll.offsetLeft + (scroll.getBoundingClientRect().width)) > scrollLinePosRight ){
        // isDrag = false
        console.log('вышел за границу вправо')
        scroll.style = `left: ${scrollLinePosRight - scroll.getBoundingClientRect().width}px`
      }
    }
  });

  window.addEventListener('mouseup', function (e) {
    isDrag = false
  })

  window.addEventListener('resize', function (e) {

  })

}


