import React, {useContext} from 'react'
import "./style.sass"
import Slider from "react-slick";
import CardJob from "../../Card-job/Card-job";
import {MainContext} from "../../../context/main"
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

const Jobs = () => {

  const {loadingVacancy} = useContext(MainContext)

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    className: 'jobs-carousel carousel-custom-arrow',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  };

  if (loadingVacancy) {
    return (
      <SkeletonTheme baseColor="#DCDCDC" highlightColor="#fff">
        <Skeleton className="vacancy-skeleton mt-10 mb-10" count={1}/>
      </SkeletonTheme>
    )
  }

  return (
    <Slider {...settings}>
        {/*<div>1</div>*/}
        {/*<div>2</div>*/}
        {/*<div>3</div>*/}
        {/*<div>4</div>*/}
      <CardJob/>

   </Slider>
  );
};

export default Jobs;
