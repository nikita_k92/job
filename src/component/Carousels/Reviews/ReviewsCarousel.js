import React, {useEffect, useState} from 'react';
import api from '../../../api/api'
import ReviewPhoto from '../../../assets/images/review.jpg'
import "./style.sass"
import Slider from "react-slick";
import Skeleton, {SkeletonTheme} from "react-loading-skeleton";

const ReviewsCarousel = () => {

  const [reviews, setReviews] = useState(null);
  const [loading, setLoading] = useState(true)

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    className: 'review-carousel',
    // centerMode: true,
    response:[
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]

  };

  const init =()=>{
    api.getReviews().then(res =>{
      console.log('ITEMS',res.data.ITEMS)

      setReviews(res.data.ITEMS)
    }).catch(e =>{

    }).finally(e=>{
      setLoading(false)
    })
  }

  useEffect(()=>{
    init()
  },[])


  if(loading) return (
    <SkeletonTheme baseColor="#DCDCDC" highlightColor="#fff">
      <Skeleton style={{height: '300px'}} count={1}/>
    </SkeletonTheme>
  )

  return (
    <Slider {...settings}>
      {reviews ? Object.keys(reviews).map(key =>{
        return(
          <div className="review-carousel__slide review-carousel-slide">

            <div className="review-carousel-slide__review">
              <p className="review-carousel-slide__name">
                {reviews[key].NAME}
              </p>
              <p className="review-carousel-slide__post">
                {reviews[key].PROPERTY_POST_VALUE}, <span>стаж  {reviews[key].PROPERTY_EXPERIENCE_VALUE}</span>
              </p>
              <p className="review-carousel-slide__text mb-0">
                {reviews[key].PREVIEW_TEXT}
              </p>
            </div>

            <div className="review-carousel-slide__photo">
              <img
                src={reviews[key].PREVIEW_PICTURE}
                className="img-responsive" alt="Сотрудник"
              />
            </div>

          </div>
        )
      })
        :
        <div>
          ошибка
        </div>
      }

    </Slider>
  );
};

export default ReviewsCarousel;
