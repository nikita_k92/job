import React, {useEffect, useState, useRef} from 'react';
import Slider from "react-slick";
import ReactFancyBox from "react-fancybox";
import "react-fancybox/lib/fancybox.css";
import './style.sass'
import Life1 from "../../../assets/images/life1.jpg"
import Life2 from "../../../assets/images/life2.jpg"
import Life3 from "../../../assets/images/life3.jpg"
import Life4 from "../../../assets/images/life4.jpg"
import Life5 from "../../../assets/images/life5.jpg"
import Life6 from "../../../assets/images/life6.jpg"
import Life7 from "../../../assets/images/life7.jpg"
import Life8 from "../../../assets/images/life8.jpg"
import Photo from "../../../assets/images/photo1.jpg"

const Life = () => {
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const slider1 = useRef(null);
  const slider2 = useRef(null);

  useEffect(() => {
    setNav1(slider1.current);
    setNav2(slider2.current);
  }, []);


  return (
    <div className="slider-life-area carousel-custom-arrow">
      <Slider
        // centerMode={true}
        // variableWidth={false}
        slidesToShow={1}
        slidesToScroll={1}
        // adaptiveHeight={true}
        className="life-big"
        asNavFor={nav2}
        ref={slider1}
      >
        {/*<div>*/}
        {/*  <ReactFancyBox*/}
        {/*    showCloseBtn={false}*/}
        {/*    thumbnail={Life1}*/}
        {/*    image={Life1}*/}
        {/*  />*/}
        {/*</div>*/}

        <img src={Life1} alt=""/>

        <img src={Life2} alt=""/>

        <img src={Life3} alt=""/>

        <img src={Life4} alt=""/>

        <img src={Life5} alt=""/>
        <img src={Life6} alt=""/>
        <img src={Life7} alt=""/>
        <img src={Life8} alt=""/>

      </Slider>
      <Slider
        centerMode={false}
        className="life-small"
        asNavFor={nav1}
        ref={slider2}
        variableWidth={true}
        slidesToShow={6}
        slidesToScroll={1}
        swipeToSlide={true}
        focusOnSelect={true}
        responsive={[
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            }
          },
        ]}
      >

        <img src={Life1} alt=""/>

        <img src={Life2} alt=""/>

        <img src={Life3} alt=""/>

        <img src={Life4} alt=""/>

        <img src={Life5} alt=""/>
        <img src={Life6} alt=""/>
        <img src={Life7} alt=""/>
        <img src={Life8} alt=""/>

      </Slider>
    </div>
  );
};

export  default Life;
