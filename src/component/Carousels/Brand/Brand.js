import React, {useEffect, useState} from 'react';
import Slider from "react-slick";
import B1 from "../../../assets/images/brand-1.png";
import api from "../../../api/api"
import Skeleton, {SkeletonTheme} from "react-loading-skeleton"
import 'react-loading-skeleton/dist/skeleton.css'


const Brand = () => {


  const settings = {
    centerMode: true,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    className: 'brand',
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  };

  const [brands, setBrands] = useState([])
  const [loadBrands, setLoadBrands] = useState(true)

  useEffect(()=>{
    api.getBrands().then(resp =>{
      console.log(resp.data)

      setBrands(resp.data)
    }).catch(e=>{

    })
  },[])

  useEffect(()=>{
    if(!brands) return
    setLoadBrands(false)
  }, [brands])

  if(loadBrands) return(
    <div className="container">
      <SkeletonTheme baseColor="#DCDCDC" highlightColor="#fff">
        <Skeleton style={{height: '50px'}} count={1}/>
      </SkeletonTheme>
    </div>

  )
  return (
    <div>
      <Slider {...settings}>
        {Object.keys(brands).map((key, index)=>{
          return(
            <div className="brand__slide" key={index}>
              <img src={brands[key].PREVIEW_PICTURE} alt=""/>
            </div>
          )
        })}
      </Slider>
    </div>
  );
};

export default Brand;
