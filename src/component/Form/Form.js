import React, {Fragment, useContext, useEffect, useState, useRef} from 'react'
import "react-datepicker/dist/react-datepicker.css";
import "react-datepicker/dist/react-datepicker.css";
import ru from "date-fns/locale/ru";

import Select, {StylesConfig} from 'react-select'
import {InterviewContext} from "../../context/interview"
import InputMask from 'react-input-mask'
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import api from "../../api/api"
import Check from "../../assets/images/check.svg"
import Loading from "../../component/Loading/Loading"
import {Months, Years} from "../../utils/Constants"
import DatePicker,{registerLocale } from "react-datepicker"
import { getMonth, getYear } from 'date-fns';
registerLocale("ru", ru);



const Form = ({type = 1}) => {
  const ref = useRef();
  const [post, setPost] = useState()
  const [loadingPosition, setLoadingPosition] = useState(true)

  const [fileError, setFileError] = useState('')


  const {
    showCommentInput, setShowCommentInput,
    fio, setFio,
    phone, setPhone,
    birthday, setBirthday,
    email, setEmail,
    position, setPosition,
    file, setFile,
    comment, setComment,
    agreement, setAgreement,
    success, setSuccess,
    loadingSuccess, setLoadingSuccess,
    errorPhone, setErrorPhone,
    errorFio, setErrorFio,
    errorBirthday, setErrorBirthday
  } = useContext(InterviewContext)

  const handleSubmit = (e) => {

    e.preventDefault()
    e.stopPropagation()
    if (!agreement) return
    setLoadingSuccess(true)
    const formData = new FormData()
    formData.append('FIO', fio)
    formData.append('PHONE', phone)
    formData.append('BIRTHDAY', birthday)
    formData.append('EMAIL', email)
    formData.append('POSITION', position)
    formData.append('FILE', file)
    formData.append('COMMENT', comment)
    formData.append('ACTION', 'INTERVIEW')

    // console.log('formData', formData)
    api.getInterview(formData).then(res => {
      setSuccess(true)
      setLoadingSuccess(false)
      setFio('')
      setPhone('')
      setBirthday('')
      setEmail('')
      setPosition('')
      setFile('')
      setAgreement('')
      setComment('')
      setErrorPhone('')
      setErrorFio('')
      setErrorBirthday('')
    }).catch(e => {
      console.log('e', e)
      setErrorPhone(e.response.data.ERROR.phone)
      setErrorFio(e.response.data.ERROR.fio)
      setErrorBirthday(e.response.data.ERROR.birthday)
      setLoadingSuccess(false)
    })
  }

  useEffect(() => {
    setErrorPhone('')
    setErrorFio('')
    setLoadingPosition(true)
    api.getFields().then(resp => {
      let newArraySection = []
      Object.keys(resp.data['SECTION']).forEach(key => {
        newArraySection.push(
          {
            value: resp.data['SECTION'][key].VALUE,
            label: resp.data['SECTION'][key].VALUE,
          }
        )
      })
      setPost(newArraySection)
    }).catch(e => {

    })
  }, [])

  useEffect(() => {
    if (!post) return
    setLoadingPosition(false)
  }, [post])

  const handleCheckAgreement = (e, agr) => {
    e.preventDefault()
    e.stopPropagation()
    console.log('handleCheckAgreement')
    setAgreement(!agr)
  }

  const onFileChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      if (event.target.files[0].size > 5242880) {
        setFileError('Максимальный размер файла 5 мегабайт')
      } else {
        // console.log(event.target.files[0])
        setFile(event.target.files[0])
      }
    }
  }

  const colourStyles = {
    dropdownIndicator: (base, state) => ({
      // backgroundColor: 'blue'
    }),
    control: (base, state) => ({
      ...base,
      height: "30px",
      minHeight: "30px",
      maxHeight: "30px",
      paddingLeft: '0.625rem',
      border: "0 !important",
      boxShadow: "0 !important",
      "&:hover": {
        border: "0 !important"
      }
    })
  }

  return (
    <Fragment>
      <form className={"form " + (type)} onSubmit={handleSubmit}>
        <div className="form__group d-flex align-items-baseline flex-wrap">
          <div className="form__input-container">
            <div className={"form__field " + (fio ? 'form__no-empty' : '')}>
              <input type="text" id={"fullname-" + type}
                     className="form__input"
                     placeholder=""
                     value={fio || ''}
                     onChange={(e) => setFio(e.target.value)}
              />
              <label htmlFor={"fullname-" + type} className="form__label">ФИО*</label>
            </div>
            <div className={"form__error " + (errorFio ? 'form__error-visible' : '')}>{errorFio}</div>
          </div>

          <div className="form__input-container">
            <div className={"form__field "  + (phone ? 'form__no-empty' : '')}>
              <InputMask
                id={"phone-" + type}
                // required={true}
                className="form__input"
                value={phone || ''}
                onChange={(e) => setPhone(e.target.value)}
                mask="+7(999) 999-99-99"
                maskChar={null}
              />

              <label htmlFor={"phone" + type} className="form__label">Номер телефона*</label>

            </div>
            <div className={"form__error " + (errorPhone ? 'form__error-visible' : '')}>{errorPhone}</div>
          </div>

          <div className="form__input-container">
            <div className={"form__field "  + (birthday? 'form__no-empty' : '' )}>
              <DatePicker
                locale="ru"
                className="form__input w-100 custom-date-picker"
                selected={birthday}
                onChange={(date) => setBirthday(date)}
                dateFormat="dd.MM.yyyy"
                peekNextMonth
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                renderCustomHeader={({
                                       date,
                                       changeMonth,
                                       changeYear,
                                     }) => (
                  <div>
                    <select
                      value={getYear(date)}
                      onChange={({target: {value}}) => changeYear(value)}
                    >
                      {Years.map((option) => (
                        <option key={option} value={option}>
                          {option}
                        </option>
                      ))}
                    </select>
                    <select
                      // value={Months[getMonth(date)]}
                      onChange={({target: {value}}) =>
                        changeMonth(Months.indexOf(value))
                      }
                    >
                      {Months.map((option) => (
                        <option key={option} value={option}>
                          {option}
                        </option>
                      ))}
                    </select>
                  </div>

                )}
              />
              <label htmlFor={"date-" + type} className="form__label">Дата рождения*</label>
            </div>
            <div className={"form__error " + (errorBirthday ? 'form__error-visible' : '')}>{errorBirthday}</div>
          </div>

        </div>

        <div className="form__group d-flex align-items-baseline flex-wrap">

          <div className="form__input-container ">
            <div className="form__field">
              <input type="text"
                     id={"email-" + type}
                     value={email || ''}
                     onChange={(e) => setEmail(e.target.value)}
                // className="form__input form__input-error"
                     className="form__input"
              />
              <label htmlFor={"email-" + type} className="form__label">Email</label>
            </div>
          </div>

          <div className="form__input-container form__input-container-error" style={{maxWidth: '500px'}}>

            {loadingPosition
              ?
              <SkeletonTheme baseColor="#f3f3f3" highlightColor="#ecebeb">
                <Skeleton style={{height: '30px'}} count={1}/>
              </SkeletonTheme>
              :
              <div className="form__field">

                <div className="position-relative">
                  <Select
                    styles={colourStyles}
                    id={"post-" + type}
                    defaultValue={position}
                    placeholder={type !== 'form-main' ? <div className="select-placeholder">Выберите должность</div> : ''}
                    onChange={e => setPosition(e.value)}
                    className="form__input simple-select"
                    options={post}/>
                </div>
                <label htmlFor={"post-" + type}
                       className="form__label">Желаемая должность</label>
              </div>
            }

          </div>

        </div>
        {type === 'form-modal' && showCommentInput
        &&
        <div className="form__group d-flex align-items-baseline flex-wrap">
          <div className="form__input-container ">
            <div className="form__field">
              <input type="text"
                     id={"comment-" + type}
                     value={comment || ''}
                     onChange={(e) => setComment(e.target.value)}
                // className="form__input form__input-error"
                     className="form__input"
                     placeholder="Комментарий"/>
              <label htmlFor={"email-" + type} className="form__label">Комментарий</label>

            </div>
          </div>
        </div>
        }


        <div className="form__warning form__group ml-0 mr-0 d-flex justify-content-between flex-wrap">
          <div className="form__text-red">
            <span className="text-red">Оставьте</span> свою почту, чтобы получить рекомендации для подготовки к
            собеседованию
          </div>
          <div className="form__text-blue">* Поля, обязательные для заполнения</div>
        </div>
        <div
          className="form__group form__group-mb40 ml-0 mr-0 d-flex justify-content-between align-items-center flex-wrap">
          <>
            {!file
              ?
              <button className="button button__file mt-5 mb-5">
                <input type="file" className="button__input" onChange={onFileChange}/>
                Прикрепите файл (pdf, docx)
              </button>
              :
              <div className="form__text-blue d-flex align-items-center flex-wrap w-100">
                <span className="mr-10">{file.name}</span>
                <button className="button button__file mt-5 mb-5 ml-auto" onClick={(e) => setFile('')}>
                  Удалить файл
                </button>
              </div>
            }

            {fileError && <div className="mt-5 form__error form__error-visible">{fileError}</div>}

          </>

          <div>
            <div className="contact-block__info form__info">

              <div className="checkbox-fade fade-in-primary checkbox-fade--mb-10 checkbox-fade--float-right"
                   onClick={(e) => handleCheckAgreement(e, agreement)}
              >
                <label className="check-task align-items-center">
                  <input type="checkbox" checked={agreement} readOnly/>
                  <span className="cr ml-5 mr-5 order-2">
                          <img src={Check} alt=""/>
                        </span>
                  <span className="order-1">
                        Я согласен с условиями передачи данных. <a href="">Соглашение</a>.
                        </span>
                </label>
              </div>
            </div>
          </div>


        </div>
        <div
          className="form__button-area form__warning form__group ml-0 mr-0 mb-0 d-flex justify-content-center flex-wrap">
          {!loadingSuccess
            ?
            <button
              type="submit"
              className={"button button__red form__button__red " + (!agreement ? 'button__red--no-click' : '')}>
              Отправить анкету
            </button>
            :
            <div className="loading form__loading">
              <Loading/>
            </div>
          }

        </div>
      </form>
    </Fragment>
  )
}

export default Form
