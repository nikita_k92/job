import React, {useContext} from 'react';
import FooterLogo from "../../assets/images/Logo.svg"
import {Constants} from "../../utils/Constants"
import "./footer.sass"
import {MainContext} from "../../context/main";
import {InterviewContext} from "../../context/interview";
import {Link} from "react-router-dom"

const Footer = () => {

  const {formModal, setFormModal} = useContext(MainContext)
  const {showCommentInput, setShowCommentInput} = useContext(InterviewContext)

  const list = [
    {
      title: "Коллектив",
      links: [
        {
          text: 'Руководство',
          link: '/team',
        },
        {
          text: 'Наши истории',
          link: '/history',
        },
        {
          text: 'Вакансии',
          link: '/vacancies',
        },
        {
          text: 'Преимущества',
          link: '/advantages',
        },
        {
          text: 'Контакты',
          link: '/contact',
        }
      ]
    },
    // {
    //   title: "О компании",
    //   links: [
    //     {
    //       text: 'Руководство',
    //       link: '#',
    //     },
    //     {
    //       text: 'История',
    //       link: '#',
    //     },
    //     {
    //       text: 'Работа с нами',
    //       link: '#',
    //     },
    //     {
    //       text: 'Сайт компании',
    //       link: '#',
    //     }
    //   ]
    // },
  ]

  const clickButton =()=>{
    setShowCommentInput(true)
    setFormModal(!formModal)
  }

  return (
    <div className="footer">
      <div className="container">
        <div className="row">
          <div className="col-xl-4 col-lg-12">
              <div className="footer__block">
                <div className="footer__logo">
                  <img src={FooterLogo} alt="MTK"/>
                </div>
                <p className="footer__text">
                  Вы можете задать вопрос
                  на почту, и наш менеджер свяжется с вами через некоторое время.
                </p>
                <button onClick={e =>clickButton()} className="button button__footer">
                  Задать вопрос
                </button>
              </div>
          </div>
          <div className="col-xl-1 d-xl-inline-block d-lg-none"/>
          <div className="col-xl-7 col-lg-12">
            <div className="footer-navigation footer__navigation d-flex justify-content-xl-end justify-content-start flex-wrap">
              {list.map((item, index)=>{
                return(
                  <div
                    key={index}
                    className="footer-navigation__block">
                    <h6 className="footer-navigation__title">{item.title}</h6>
                    <ul className="footer-navigation__list">
                      {list[index].links.map((it,ind)=>{
                        return(
                          <li
                            key={ind}
                            className="footer-navigation__item">
                            <Link to={it.link} className="footer-navigation__link">
                              {it.text}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                )
              })}
              <div className="footer-navigation__block mr-0">
                <h6 className="footer-navigation__title">Контакты</h6>
                <ul className="footer-navigation__list">
                  <li className="footer-navigation__item">
                    <a href={`tel:${Constants.phone_format}`} className="footer-navigation__link footer-navigation__link--active">
                      {Constants.phone_text}
                    </a>
                  </li>
                  <li className="footer-navigation__item">
                    <a  href={`tel:${Constants.phone_2_format}`} className="footer-navigation__link">
                      {Constants.phone_2_text}
                    </a>
                  </li>

                  <li className="footer-navigation__item">
                    <a href={`mailto:${Constants.email2}`} className="footer-navigation__link">
                      {Constants.email2}
                    </a>
                  </li>
                  <li className="footer-navigation__item">
                    <a href={`mailto:${Constants.email}`} className="footer-navigation__link">
                      {Constants.email}
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-line footer__line ">
        <div className="container">
          <div className="row">
            <div className="col-12 d-flex justify-content-between align-items-center flex-wrap">
              <p className="footer-line__text">©2009-2021 МТК Росберг_Вакансии. Шины и запчасти для строительной и
                сельхозтехники.</p>
              <a href="#" className="footer-line__text">
                Пользовательское соглашение
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
