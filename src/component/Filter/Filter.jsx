import React, {useContext, useEffect, useState, useRef} from 'react'
import {useHistory, useLocation} from 'react-router-dom'
import {CategoryType} from '../../utils/Constants'
import Skeleton, {SkeletonTheme} from "react-loading-skeleton"
import api from "../../api/api"
import {MainContext} from "../../context/main"
import CloseFilterDrop from "../../assets/images/close-filter-drop.png"
import Vector from "../../assets/images/vector.svg"
import ArrowRed from '../../assets/images/arrow-red.svg'
import Check from '../../assets/images/check.svg'
import {Declination, NormalizePaginationView, NormalizeVacancyView} from "../../utils/Helpers"

const Filter = () => {

  const {
    vacancies,
    categories,
    loadingCategory,
    setLoadingVacancy,
    checkCategory,
    setVacancies,
    setPagination,
    filterCountVacancy,
    filterLink,
  } = useContext(MainContext)

  const [positionY, setPositionY] = useState(0)
  const [filterBlock, setFilterBlock] = useState([])
  //количество вакансий для выпадашки в блоке фильтр
  const [countVacancies, setCountVacancies] = useState(0)

  let history = useHistory()
  let location = useLocation()

  const wrapperRef = useRef(null);

  useOutsideAlerter(wrapperRef);

  const showVacancyFilter = (e) => {
    // e.preventDefault()
    // e.stopPropagation()
    setPositionY(null)
    history.push(`${location.pathname}?PAGE=1${filterLink}`)
    setLoadingVacancy(true)
    api.getVacancyFilter(filterLink).then(resp => {
      setPagination(NormalizePaginationView(resp.data))
      setVacancies(NormalizeVacancyView(resp.data.ITEMS))
    }).catch(error => {
      console.log('resp', error)
    })
  }

  const deleteAllTags = () => {
    checkCategory()
    api.getAllVacancies().then(resp => {
      setPagination(NormalizePaginationView(resp.data))
      setVacancies(NormalizeVacancyView(resp.data.ITEMS))
      history.push('/vacancies')
    }).catch(error => {
      console.log('resp', error)
    })
  }

  const openCloseFilterBlock = (key) => {
    setPositionY(0)
    let x = {...filterBlock}
    if (key in x) {
      delete x[key]
    } else {
      x[key] = true
    }
    setFilterBlock(x)
  }

  const handleCoordinateMouse = (e, item, itemType) => {
    e.preventDefault()
    e.stopPropagation()
    checkCategory(item)
    setPositionY(e.nativeEvent.layerY - 14)
  }

  useEffect(() => {
    api.getVacancyFilter(filterLink).then(resp => {
      setCountVacancies(Object.keys(resp.data.ITEMS).length)
      // console.log(Object.keys(resp.data.ITEMS).length)
    }).catch(error => {
      console.log('resp', error)
    })
  }, [filterLink])

  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if(event.target.className === 'filter-drop-block__result') return
        if (ref.current && !ref.current.contains(event.target)) {
          setPositionY(0)
        }
      }
      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  const refVacancies = useRef(null)

  useEffect(() => {

    if( window.matchMedia("(max-width: 992px)").matches ){
      refVacancies.current.scrollIntoView({behavior: 'smooth', block: 'start'})
    }
  }, [vacancies])

  const blockCategory = () => {

    return categories?.map((item, index) => {
      return (
        <div
          className={"filter__block filter-block " + (filterBlock[item.type] ? 'filter-block--close' : '')}
          key={index}>
          <div
            onClick={e => openCloseFilterBlock(item.type)}
            className="filter-block__title d-flex align-items-center justify-content-between">
            <span>{CategoryType[item.type]}</span>
            <div
              className="filter-block__arrow ml-5 d-flex align-items-center">
              <img src={ArrowRed} alt="открыть/закрыть"/>
            </div>
          </div>
          <div className="filter-block__list">
            <div className="filter-block__item">
              {item?.items?.map((it, ind) => {
                return (
                  <div className="checkbox-fade fade-in-primary checkbox-fade--mb-10"
                       onClick={e => handleCoordinateMouse(e, it, item.type)}
                       key={ind}>
                    <label className="check-task">
                      <input type="checkbox" checked={!!it.check} readOnly/>
                      <span className="cr">
                          <img src={Check} alt=""/>
                        </span>
                      <span>{it.value}
                        <span className="count">{it.count}</span>
                        </span>
                    </label>
                  </div>
                )
              })}
            </div>
          </div>

          {/*{categories[key].length > 2 && !categories[key].open*/}
          {/*&&*/}
          {/*<button className="button filter-block__button filter__button">*/}
          {/*Показать еще*/}
          {/*</button>*/}
          {/*}*/}
          {/*{categories[key].length > 2 && categories[key].open*/}
          {/*&&*/}
          {/*<button className="button filter-block__button filter__button">*/}
          {/*Скрыть*/}
          {/*</button>*/}
          {/*}*/}

        </div>
      )
    })
  }

  return (
    <div className="filter mb-20">
      <div

        style={{top: `${positionY}px`}}
        className={"filter-drop-block " + (positionY ? 'd-block' : 'd-none')}>
        <div className="filter-drop-block__body">
          <div className="filter-drop-block__vector">
            <img src={Vector} alt=""/>
          </div>
          <div className="filter-drop-block__close" onClick={e => setPositionY(null)}>
            <img src={CloseFilterDrop} alt=""/>
          </div>
          <div className="filter-drop-block__title">Поиск вакансий</div>
          <div

            className="filter-drop-block__result"
            onClick={(e) => showVacancyFilter(e)}>
            Найдены {countVacancies}&nbsp;{Declination(countVacancies, ['вакансия', 'вакансии', 'вакансий'])}
          </div>
        </div>
      </div>

      <div className="mb-10">
        <div className="filter__title">Фильтры</div>
        <button
          onClick={() => deleteAllTags()}
          className="button filter__button">
          Очистить все
        </button>
      </div>
      {loadingCategory
        ?
        <SkeletonTheme baseColor="#f3f3f3" highlightColor="#ecebeb">
          <Skeleton style={{minHeight: '200px'}} count={1}/>
        </SkeletonTheme>
        :

        <div ref={wrapperRef}>
          {blockCategory()}
        </div>

      }
      <div style={{opacity: 0}} ref={refVacancies}/>
      <button
        onClick={(e) => showVacancyFilter(e)}
        className="button button__transparent button-filter-search">Показать вакансии
      </button>

    </div>
  )
}

export default Filter
