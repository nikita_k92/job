import React, {useContext, useEffect, useState} from 'react';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import Point from "../../assets/images/point.svg";
import {TypeJob} from "../../utils/Constants";
import api from "../../api/api";
import {MainContext} from "../../context/main";
import moment from "moment";
import {Link} from "react-router-dom";
import {NormalizeVacancyView, price} from "../../utils/Helpers"

const CardJob = () => {

  const {vacancies, setVacancies,
    formModal, setFormModal,
    setLoadingVacancy, loadingVacancy} = useContext(MainContext)

  // const [loadingVacancy, setLoadingVacancy] = useState(true)

  useEffect(() => {
    api.getAllVacancies().then(resp => {
      setVacancies(NormalizeVacancyView(resp.data.ITEMS))
    }).catch(error => {
      console.log('resp', error)
    })
  }, []);

  useEffect(() => {
    if (!vacancies) return
    setLoadingVacancy(false)
    // console.log(vacancies)
  }, [vacancies])

  if (loadingVacancy) {
    return (
      <SkeletonTheme baseColor="#DCDCDC" highlightColor="#fff">
        <Skeleton className="vacancy-skeleton mt-10 mb-10" count={1}/>
      </SkeletonTheme>
    )
  }

  return (
    vacancies.slice(0, 3).map((item, index) => {
      return (
        <div className="col-xl-4 col-lg-6 col-12" key={item.ID}>
          <div className="card-job">
            <div className="card-job__header d-flex justify-content-between align-items-center">
              <div className="card-job__header__map d-flex align-items-center">
                <img className="card-job__header__img" src={Point} alt=""/>
                <span className="card-job__header__text">{item.CITY.VALUE}</span>
              </div>
              <div className={"card-job__header--status status status_" + (TypeJob[item.CHART.ENUM_ID].class)}>
                {TypeJob[item.CHART.ENUM_ID].text}
              </div>
            </div>
            <div className="card-job__job" >
              <Link to={"/vacancy/" + item.ID} style={{color: '#38484e'}}>
                {item.NAME}
              </Link>
            </div>


            <div className="card-job__salary">
              {item.PAY}
              {/*<span className="rub">₽</span>*/}
              {/*<span className="from">от</span>30 000 <span className="rub">₽</span>*/}
            </div>
            <div className="card-job__date">
              {moment(item.ACTIVE_FROM.split(' ')[0], 'DD.MM.YYYY').format('LL')}
            </div>
            <div className="card-job__buttons d-flex justify-content-between">
              <Link to={"/vacancy/" + item.ID}
                    className="button card-job__blue button__blue">Подробнее
              </Link>
              <button
                onClick={e => setFormModal(!formModal)}
                className="button card-job__transparent button__transparent">Отправить резюме</button>
            </div>
          </div>
        </div>
      )
    })
  );
};

export default CardJob;
