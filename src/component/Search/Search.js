import React, {useEffect, useState, useMemo, useContext} from 'react';
import PropTypes from 'prop-types';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import SearchIcon from "../../assets/images/search.png"
import Point from "../../assets/images/point.svg";
import Select from 'react-select'
import {useLocation, useHistory } from "react-router-dom";
import { parse } from 'query-string';
import api from "../../api/api";
import {InterviewContext} from "../../context/interview";


const Search = ({modification}) => {

  let history = useHistory();

  const [searchField, setSearchField] = useState('');
  const [regionField, setRegionField] = useState('');
  const [regionIndex, setRegionIndex] = useState(null)
  const [load, setLoad] = useState(true)

  //Все регионы
  const [cities, setCities] = useState([])
  //Регион который выбрали //от сюда берем код
  const [city, setCity] = useState(null)

  const [loadingPosition, setLoadingPosition] = useState(true)

  // const {city, setCity} = useContext(InterviewContext)/

  let location = useLocation()

  useEffect(()=>{
    let queryParams = parse(location.search)
    setSearchField(queryParams?.search ? decodeURI(queryParams?.search) : '')
    setRegionField(decodeURI(queryParams?.region))
    setLoad(false)
  },[])

  const Submit =(e)=>{
    e.preventDefault();
    e.stopPropagation()

    history.push(`vacancies?FIND=${encodeURI(searchField)}&CITY[]=${encodeURI(city ? city : '')}`);
  }

  useEffect(() => {
    setLoadingPosition(true)
    api.getFields().then(resp => {
      let newArraySection = []
      Object.keys(resp.data['CITY']).forEach(key => {
        newArraySection.push(
          {
            value: resp.data['CITY'][key].VALUE,
            label: resp.data['CITY'][key].VALUE,
            code: resp.data['CITY'][key].ID,
          }
        )
      })
      setCities(newArraySection)
    }).catch(error => {
      console.log('resp', error)
    })
  }, []);

  const changeCity =(item)=>{
    setCity(item.code)

  }
  console.log(cities)
  if(load) return (
    <SkeletonTheme baseColor="#f3f3f3" highlightColor="#ecebeb">
      <Skeleton className="search-skeleton" count={1} />
    </SkeletonTheme>
  )

  return (
    <form className="search d-flex" onSubmit={Submit}>
      <div className={"search__form d-flex w-100 " + (modification)}>
        <div className="search__input d-flex align-items-center">
          <img className="search__icon" src={SearchIcon} alt="Поиск"/>
          <input type="text"
                 onChange={e=>setSearchField(e.target.value)}
                 value={searchField}
                 className="form-control"
                 placeholder="Введите желаемую должность"/>
        </div>

        <div className="search__select d-flex align-items-center">
          <img className="search__icon" src={Point} alt="Карта"/>

          <Select
            placeholder={<div>Выберите регион</div>}
            defaultValue={cities[city]}
            onChange={e => changeCity(e)}
            className="simple-select"
            options={cities}/>
        </div>

      </div>
      <button className="button button__red search__button">Найти вакансию</button>
    </form>
  );

};

Search.propTypes = {
  modification: PropTypes.string
};

export default Search;
