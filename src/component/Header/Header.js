import React, {useContext, useEffect, useState} from 'react';
import {Link, useLocation} from "react-router-dom";
import '../../assets/styles/style.sass'
import Logo from "../../assets/images/Logo.svg";
import {slide as Menu} from 'react-burger-menu'
import {MainContext} from "../../context/main";

const Header = () => {

  const {formModal, setFormModal} = useContext(MainContext)
  const [isOpen, setIsOpen] = useState(false)

  let location = useLocation()
  // console.log()
  const links = [
    {
      title: 'О КОМПАНИИ',
      route: '/',
    },
    {
      title: 'ВАКАНСИИ',
      route: '/vacancies'
    },
    {
      title: 'КОЛЛЕКТИВ',
      route: '/team'
    },
    {
      title: 'Преимущества',
      route: '/advantages'
    },
    {
      title: 'КОНТАКТЫ',
      route: '/contact'
    }
  ]

  useEffect( () => {
    setIsOpen(false)
  },[location.pathname] );

  return (
    <div className="container-fluid header-area">

      <div className="container">

        <header className="d-flex w-100 flex-md-row flex-column ">
          <div className="logo-header d-flex">
            <Link to="/" className="logo-header__link d-flex align-items-center">
              <img className="logo-header__img" src={Logo} alt=""/>
            </Link>
          </div>

          <nav className="menu d-xl-block d-none">
            <ul className="menu__list  d-flex h-100">
              {links.map((item, index) => {
                return (
                  <li key={index} className={"menu__item " + (location.pathname === item.route ? "menu__item--active" : "")}>
                    <Link to={item.route} className="menu__link h-100 d-flex align-items-center">
                      <span>{item.title}</span>
                    </Link>
                  </li>
                )
              })}
            </ul>
          </nav>

          <div className="interview d-flex align-items-center">

            <div
              className="burger-menu d-flex align-items-center"
              onClick={e=>setIsOpen(!isOpen)}/>

            <Menu burgerButtonClassName={"my-burger-menu d-xl-none d-block"} isOpen={ isOpen }>
              <a href="#" className="logo-header__link d-flex align-items-center mb-30">
                <img className="logo-header__img img-responsive" src={Logo} alt=""/>
              </a>
              <ul className="mt-25">
                {links.map((item, index) => {
                  return (
                    <li key={index} className={"menu__link ml-0 mb-15 menu__item " + (location.pathname === item.route ? "menu__item--active" : "")}>
                      <Link style={{height: '20px'}}
                            onClick={e=>setIsOpen(!isOpen)}
                            to={item.route} className="menu__link d-inline-block ml-0">
                        <span style={{top: 'auto'}}>{item.title}</span>
                      </Link>
                    </li>
                  )
                })}
              </ul>
            </Menu >

            <button onClick={e => setFormModal(!formModal)} className="button button__white mt-2 mb-2">
              Записаться на собеседование
            </button>
          </div>
        </header>
      </div>
    </div>
  );
};

export default Header;
