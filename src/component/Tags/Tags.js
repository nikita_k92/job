import React, {useContext, useEffect} from 'react'
import Close from "../../assets/images/close.png"
import {MainContext} from "../../context/main"

const Tags = () => {

  const {categories, checkCategory} = useContext(MainContext)

  const listTags = categories?.map((item, index) => {
    return item?.items.map((it, ind) => {
      if (it.check) {
        return (
          <button className="tags__tag tag-jod" key={ind}>
            <span className="tag__text">
              {it.value}
            </span>
            <img className="tag__close"
                 src={Close}
                 alt=""
                 onClick={e => checkCategory(it)}/>
          </button>
        )
      }
    })
  })

  const showLine =()=>{

  }

  useEffect(()=>{

  },[categories])



  return (
    <>
      <div className="tags d-flex flex-wrap">
        {listTags}
      </div>

      {listTags
      &&
      <hr className="tags-line"/>
      }
    </>
  )
}

export default Tags

