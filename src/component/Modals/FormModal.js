import React, {useContext, useState} from 'react';
import "./style.sass"
import {Modal, Button} from 'react-bootstrap';
import Form from '../../component/Form/Form'
import {MainContext} from "../../context/main";
import {InterviewContext} from "../../context/interview"
import FormSuccess from "../../component/FormSuccess/FormSuccess"

const FormModal = () => {

  const {formModal, setFormModal} = useContext(MainContext)

  const {
    showCommentInput, setShowCommentInput,
    success, setSuccess,
    loadingSuccess, setLoadingSuccess
  } = useContext(InterviewContext)

  const closeModal =()=>{
    setShowCommentInput(false)
    setFormModal(false)
  }

  return (
    <>
      <Modal
        centered
        contentClassName="modal-interview"
        show={formModal}
        onHide={e =>closeModal()}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          {!success
          &&
          <Modal.Title>Записаться на собеседование</Modal.Title>
          }
        </Modal.Header>
        <Modal.Body>
          {success
            ?
            <FormSuccess/>
            :
            <Form type="form-modal"/>
          }
        </Modal.Body>
      </Modal>
    </>
  );
};

export default FormModal;
