import axios from 'axios'

export default {
  getAllVacancies : () => axios.get('/api/hr/find/'),
  getVacancy : (id) => axios.get(`/api/hr/find/?COUNT=1&ID=${id}`),
  // getFields : () => axios.get('/api/hr/getFields/'),
  getFields : () => axios.get('/api/hr/filter/'),
  getVacancyFilter: (link) => axios.get(`/api/hr/find/?${link}`),
  getVacancyName: (name) => axios.get(`/api/hr/find/?FIND=${name}`),
  getInterview: (data) => axios.post(`/api/hr/interview/`, data),
  getBrands: () => axios.post(`/api/hr/brands/`),
  getReviews: () => axios.get(`/api/hr/reviews/get/`)
}
