// import React from 'react';
// import axios from 'axios'
//
// export default url => {
//
//   const [isLoading, setIsLoading] = useState(false)
//   const [response, setResponse] = useState(null)
//   const [error, setError] = useState(null)
//   const [data, setData] = useState({})
//   //useCallback используют для кэширования кастомных хуков
//   //массив зависимости в этом случае должен быть пустой
//   const doFetch = useCallback((data = {}) => {
//     // console.log("url", url)
//     setData(data)
//     setIsLoading(true)
//   }, [])
//
//   useEffect(() => {
//     // const requestData = {
//     //   ...data,
//     //   ...{
//     //     headers: {
//     //       Authorization: token ? `Bearer ${token}` : ''
//     //     }
//     //   }
//     // }
//     // if (!isLoading) {
//     //   return
//     // }
//
//     axios(baseUrl + url, requestData)
//       .then(res => {
//         // console.log("res", res);
//         setResponse(res.data)
//         setError(null)
//         setIsLoading(false)
//       })
//       .catch(error => {
//         setError(error.response)
//         setIsLoading(false)
//       })
//   }, [isLoading, url, data, token, baseUrl])
//
//   return [{isLoading, response, error, baseUrl}, doFetch]
// }
