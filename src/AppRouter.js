import React from 'react'
import axios from "axios"
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import { QueryParamProvider } from 'use-query-params';
import Header from "./component/Header/Header"
import Main from "./pages/Main"
import OurHistory from "./pages/OurHistory"
import Advantages from "./pages/Advantages"
import ErrorPage from "./pages/ErrorPage"
import Elements from "./pages/Elements"
import PrivacyPolicy from "./pages/PrivacyPolicy"
import Contact from "./pages/Contact"
import Search from "./pages/Search"
import Team from "./pages/Team"
import Jobs from "./pages/Jobs"
import Job from "./pages/Job"
import Footer from "./component/Footer/Footer"
import FormModal from "./component/Modals/FormModal"
import ScrollToTop from "./hooks/ScrollToTop"

const AppRouter = () => {

  axios.defaults.baseURL = 'https://rest.mtkrosberg.ru'

  return (
    <BrowserRouter>
      <QueryParamProvider ReactRouterRoute={Route}>
        <main id="page-wrap">
          <FormModal/>
          <div className="container-fluid  p-0 m-0 d-flex flex-column ">
            <Header/>
            <ScrollToTop/>

            <Switch>
              <Route exact path="/" component={Main}/>
              <Route exact path="/history" component={OurHistory}/>
              <Route exact path="/advantages" component={Advantages}/>
              <Route exact path="/elements" component={Elements}/>
              <Route exact path="/privacy" component={PrivacyPolicy}/>
              <Route exact path="/contact" component={Contact}/>
              <Route exact path="/search" component={Search}/>
              <Route exact path="/team" component={Team}/>
              <Route path="/vacancies" component={Jobs}/>
              <Route exact path="/vacancy/:id" component={Job}/>
              <Route component={ErrorPage}/>
            </Switch>

            <Footer/>
          </div>
        </main>
      </QueryParamProvider>
    </BrowserRouter>
  )
}

export default AppRouter
