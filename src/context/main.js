import React, {createContext, useEffect, useState} from 'react'
import {CategoryDic} from "../utils/Constants"

export const MainContext = createContext()

export const MainContextProvider = ({children}) => {

  const [formModal, setFormModal] = useState(false)

  const [loadingVacancy, setLoadingVacancy] = useState(true)
  const [loadingCategory, setLoadingCategory] = useState(true)

  const [vacancies, setVacancies] = useState(null)
  const [vacancy, setVacancy] = useState(null)
  const [pagination, setPagination] = useState(null)

  const [categories, setCategories] = useState(null)

  const [cities, setCities] = useState([])

  const [filterLink, setFilterLink] = useState('')


  //функция для проставления и удаления checkbox
  //Очишения всех checkbox
  //даление тегов
  const checkCategory = (item = null) => {
    //делаем копию данных, они только так изменяются
    let link = ''
    let copy = categories.slice()
    copy.forEach((category, index) => {
      category.items.forEach(ct => {
        if (item === null) {
          ct.check = false
        }
        if (item?.id === ct.id && item.check) {
          ct.check = false
        } else if (item?.id === ct.id && !item.check) {
          ct.check = true
        }
        //формирование ссылки при клике на фильтр,
        //сслыка формируется для запроса вакансий и получения количества
        //вакансий по выбранным фильтрам
        if(ct.check){
          link += `&${CategoryDic[category.type]}=${ct.id}`
        }
      })
    })

    setFilterLink(link)
    setCategories(copy)
  }

  const createLinkWithParams = (queryParams, numberPage = null) => {
    let link = ''
    if (queryParams) {
      Object.keys(queryParams).forEach(key => {

        if (queryParams[key] !== undefined) {
          if (key === 'CITY' || key === 'SECTION' || key === 'CHART' || key === 'DECORATION') {
            queryParams[key].forEach(item => {
              link += `&${CategoryDic[key]}=${item}`
            })
          } else {
            //если параметр не массив
            link += `&${key}=${ queryParams[key]}`
          }
        }
      })
      //для пагинации переход
      if(numberPage){
        link += `&PAGE=${numberPage}`
      }
    }
    // console.log('link', link)
    return link
  }
  return (
    <MainContext.Provider
      value={{
        formModal, setFormModal,
        vacancy, setVacancy,
        loadingVacancy, setLoadingVacancy,
        loadingCategory, setLoadingCategory,
        vacancies,
        categories, setCategories,
        cities, setCities,
        filterLink, setFilterLink,
        pagination, setPagination,
        setVacancies,
        createLinkWithParams,
        checkCategory,
      }}
    >
      {children}
    </MainContext.Provider>
  )

}
