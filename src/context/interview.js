import React, {createContext, useState} from 'react';

export const InterviewContext = createContext()

export const InterviewContextProvider = ({children})=>{

  const [showCommentInput, setShowCommentInput] = useState(false);
  const [fio, setFio] = useState('');
  const [phone, setPhone] = useState('');
  const [birthday, setBirthday] = useState('');
  const [email, setEmail] = useState('');
  const [position, setPosition] = useState('');
  const [file, setFile] = useState('');
  const [comment, setComment] = useState('');
  const [errorFio, setErrorFio] = useState('');
  const [errorPhone, setErrorPhone] = useState('');
  const [errorComment, setErrorComment] = useState('');
  const [errorBirthday, setErrorBirthday] = useState('');

  const [agreement, setAgreement] = useState(false)

  //при отправки формы становится true , то вместо кнопки в форме появляется загрузка
  //становится false когда приходит ответ от сервера
  const [loadingSuccess, setLoadingSuccess] = useState(false)
  //если true то скрывается форма и появляется заглушка с успешной отправкой формы
  const [success, setSuccess] = useState(false)

  return(
    <InterviewContext.Provider value={{
      showCommentInput, setShowCommentInput,
      fio, setFio,
      phone, setPhone,
      birthday, setBirthday,
      email, setEmail,
      position, setPosition,
      file, setFile,
      agreement, setAgreement,
      success, setSuccess,
      comment, setComment,
      loadingSuccess, setLoadingSuccess,
      errorFio, setErrorFio,
      errorPhone, setErrorPhone,
      errorComment, setErrorComment,
      errorBirthday, setErrorBirthday
    }}>
      {children}
    </InterviewContext.Provider>
  )

}
