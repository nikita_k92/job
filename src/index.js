import React, {useContext, useEffect} from 'react'
import ReactDOM from 'react-dom'
import AppRouter from "./AppRouter";
import 'normalize.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/styles/main.sass'
import './assets/styles/menu.css'
import {MainContextProvider} from "./context/main"
import {InterviewContextProvider} from "./context/interview";

const App = () => {
  return (
    <MainContextProvider>
      <InterviewContextProvider>
        <AppRouter/>
      </InterviewContextProvider>
    </MainContextProvider>
  )
}

ReactDOM.render(
    <App/>,
  document.getElementById('root')
);

