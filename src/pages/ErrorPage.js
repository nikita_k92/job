import React from 'react';
import ErrorImg from "../assets/images/error_404.png"

const ErrorPage = () => {
  return (
    <div  className="page">
      <section className="page__error">
        <div className="container">
          <div className="row error-block align-items-center">
            <div className="col-lg-5 col-12 text-center">
              <img className="error-block__img" src={ErrorImg} alt=""/>
            </div>
            <div className="col-lg-7 col-12">
              <div className="error-block__info">
                <span className="error-block__title">Страница не найдена</span>
                <p className="error-block__text">Но не волнуйтесь! Наш робот ROSBERGTRON
                  поможет вам ее отыскать.
                </p>
                <a href="/" className="button button__red button--page__error">
                  Вернуться на сайт
                </a>
              </div>
            </div>
          </div>
        </div>

      </section>
    </div>
  );
};

export default ErrorPage;
