import React from 'react';
import Rocket from "../assets/images/roket.png"
import Photo1 from "../assets/images/1.png"
import Photo2 from "../assets/images/2.png"
import Photo3 from "../assets/images/3.png"
import Photo4 from "../assets/images/4.png"
import Photo5 from "../assets/images/5.png"
import Photo6 from "../assets/images/6.png"
import Photo7 from "../assets/images/7.png"
import Map from "../assets/images/map.jpg"
import Flag from "../assets/images/flag.png"
import Flag1 from "../assets/images/flag-1.png"
import Flag2 from "../assets/images/flag-2.png"
import Flag3 from "../assets/images/flag-3.png"
import Flag4 from "../assets/images/flag-4.png"
import Brand from "../component/Carousels/Brand/Brand";

const OurHistory = () => {

  return (
    <div className="page">
      <section className="page__history">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--md title-block--page__history">
                <h2 className="title-block__text">
                  <b>Наша история</b>
                </h2>
              </div>
              <p className="info-text info-text--page__history">
                Компания MTK ROSBERG была основана в конце 2009 года, начав свою деятельность с поставок запасных частей
                для иностранной сельскохозяйственной техники из Европы.
                Сфера поставок охватывала только центрально-федеральный округ, в команде было 2 человека, а площадь
                склада составляла всего 40 кв. метров.
              </p>
            </div>
          </div>
        </div>
      </section>

      <section className="page__history-history-board">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="history-board d-flex flex-xl-row flex-column">

                <div className="history-board__col d-flex flex-column">
                  <div className="history-board--left-border pt-0">
                    <div className="history-board__date mt-0">
                      2009
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>2 человека</b></span>
                      <span className="info-2">численность сотрудников</span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>40 кв.м</b></span>
                      <span className="info-2">площадь склада</span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>ЦФО</b></span>
                      <span className="info-2">география работы</span>
                    </div>
                  </div>
                  <div className="history-board__image">
                    <div className="d-flex align-items-center justify-content-center h-100">
                      <img src={Rocket} alt=""/>
                    </div>
                  </div>
                </div>

                <div
                  className="history-board__col border-0 history-board__col--mlr-20 d-flex flex-column justify-content-center">
                  <img className="history-board__photo" src={Photo1} alt=""/>
                  <img className="history-board__photo" src={Photo2} alt=""/>
                  <img className="history-board__photo" src={Photo3} alt=""/>
                </div>

                <div className="history-board__col d-flex flex-column col-revers">
                  <div className="history-board__image  mt-0 ">
                    <div className="d-flex align-items-center justify-content-center h-100">
                      <img src={Rocket} alt=""/>
                    </div>
                  </div>
                  <div className="history-board--left-border pb-0">

                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>20 человек</b></span>
                      <span className="info-2">численность сотрудников</span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>200 кв.м</b></span>
                      <span className="info-2"><b>площадь склада</b></span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>ЦФО</b></span>
                      <span className="info-2">география работы</span>
                    </div>
                    <div className="history-board__date mb-0">
                      2011
                    </div>
                  </div>

                </div>

                <div className="history-board__col history-board__col--middle d-flex flex-column">
                  <div className="history-board--left-border pt-0">
                    <div className="history-board__date mt-0">
                      2014
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>75 человек</b></span>
                      <span className="info-2">численность сотрудников</span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>40 кв.м</b></span>
                      <span className="info-2">площадь склада</span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>ЦФО, ЮФО, ПФО</b></span>
                      <span className="info-2">география работы</span>
                    </div>
                  </div>
                  <div className="history-board__image ">
                    <div className="d-flex align-items-end justify-content-center h-100">
                      <img src={Photo4} alt=""/>
                    </div>
                  </div>
                </div>


                <div className="history-board__col d-flex flex-column col-revers">
                  <div className="history-board__image  mt-0 ">
                    <div className="d-flex align-items-center justify-content-center h-100">
                      <img src={Rocket} alt=""/>
                    </div>
                  </div>

                  <div className="history-board--left-border pb-0">
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>94 человек</b></span>
                      <span className="info-2">численность сотрудников</span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>17 000 кв.м</b></span>
                      <span className="info-2"><b>площадь склада</b></span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>Россия</b></span>
                      <span className="info-2">география работы</span>
                    </div>
                    <div className="history-board__date mb-0">
                      2017
                    </div>
                  </div>
                </div>

                <div
                  className="history-board__col  border-0 history-board__col--mlr-20 d-flex flex-column justify-content-center">
                  <img className="history-board__photo" src={Photo5} alt=""/>
                  <img className="history-board__photo" src={Photo6} alt=""/>
                  <img className="history-board__photo" src={Photo7} alt=""/>
                </div>

                <div className="history-board__col d-flex flex-column">
                  <div className="history-board--left-border pt-0">
                    <div className="history-board__date mt-0">
                      2019
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>160 человек</b></span>
                      <span className="info-2">численность сотрудников</span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>18 000 кв.м</b></span>
                      <span className="info-2">площадь склада</span>
                    </div>
                    <div className="history-board__info history-board--mb10 d-flex flex-column">
                      <span className="info-1"><b>РФ и страны тамооженного союза</b></span>
                      <span className="info-2">география работы</span>
                    </div>
                  </div>
                  <div className="history-board__image">
                    <div className="d-flex align-items-center justify-content-center h-100">
                      <img src={Rocket} alt=""/>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <p className="text-blue text-blue--page__history">
                На сегодняшний день общая площадь складов нашей компании составляет 18 000 кв. м, штат сотрудников -
                более 200 человек, география работы охватывает все регионы России,
                а также страны таможенного союза ЕАЭС.
              </p>

              <div className="statistic statistic--page__history d-flex justify-content-center">
                <div className="d-inline-block">
                  <div className="d-flex flex-md-row flex-column">
                    <div className="statistic__item d-flex flex-column align-items-center">
                      <span className="statistic__count" style={{color: "#BD0A2F"}}> >30 </span>
                      <span className="statistic__text">ПОДРАЗДЕЛЕНИЙ</span>
                    </div>
                    <div className="statistic__count statistic__count--middle d-flex flex-column align-items-center">
                      <span className="statistic__count "> 18 000 м² </span>
                      <span className="statistic__text">ОБЩАЯ ПЛОЩАДЬ СКЛАДОВ</span>
                    </div>
                    <div className="statistic__item d-flex flex-column align-items-center">
                      <span className="statistic__count d-flex flex-column align-items-center"> >250 </span>
                      <span className="statistic__text">СОТРУДНИКОВ</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <p>
                На сегодняшний день общая площадь складов нашей компании составляет 18 000 кв. м, штат сотрудников -
                более 200 человек, география работы охватывает все регионы России, а также страны таможенного союза
                ЕАЭС.
                <br/>
                <br/>
                В наличии всегда есть большой ассортимент шин и камер для сельскохозяйственной, строительной и грузовой
                техники. Постоянно ведется работа по увеличению ассортимента запасных частей, есть возможность
                осуществления поставок из стран Евросоюза по низким ценам в кратчайшие сроки.
              </p>
              <div className="d-flex justify-content-center">
                <a href="#" className="button button__transparent button-link">
                  Перейти в раздел “О компании”
                </a>
              </div>
            </div>
          </div>
          <hr/>
        </div>
      </section>
      <section className="page__brand">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--md title-block--page__history">
                <h2 className="title-block__text">
                  <b>Наши партнеры</b>
                </h2>
              </div>
            </div>
          </div>
        </div>
        <Brand/>
      </section>
      <div className="container">
        <hr/>
      </div>
      <section>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--md title-block--page__history">
                <h2 className="title-block__text">
                  <b>География компании</b>
                </h2>
              </div>
              <div className="d-flex">
                <div className="row job-map">
                  <img className="p-0" src={Map} alt=""/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="customs d-flex align-items-center flex-column ">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="customs__title text-center">
                Мы работаем во всех странах <span className="text-red">Таможенного союза!</span>
              </div>
              <div className="customs__list w-100 d-flex justify-content-center flex-wrap">
                <div className="customs__item">
                  <div className="customs__img">
                    <img src={Flag} alt=""/>
                  </div>
                  <p className="text-center">
                    Россия
                  </p>
                </div>
                <div className="customs__item">
                  <div className="customs__img">
                    <img src={Flag1} alt=""/>
                  </div>
                  <p className="text-center">
                    Казахстан
                  </p>
                </div>
                <div className="customs__item">
                  <div className="customs__img">
                    <img src={Flag2} alt=""/>
                  </div>
                  <p className="text-center">
                    Белоруссия
                  </p>
                </div>
                <div className="customs__item">
                  <div className="customs__img">
                    <img src={Flag3} alt=""/>
                  </div>
                  <p className="text-center">
                    АРМЕНИЯ
                  </p>
                </div>
                <div className="customs__item">
                  <div className="customs__img">
                    <img src={Flag4} alt=""/>
                  </div>
                  <p className="text-center">
                    КИРГИЗИЯ
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  );
};

export default OurHistory;
