import React from 'react';
import {Link} from "react-router-dom"
import Point from "../assets/images/point.svg"

const Elements = () => {
  return (
    <div className="container">

      <div className="m-5 d-flex flex-column">
        <Link to="/">
          Главная
        </Link>
        <Link to="/history">
          Наша история
        </Link>
        <Link to="/advantages">
          Преимущества
        </Link>
        <Link to="/error">
          404
        </Link>
        <Link to="/privacy">
          Политика конфиденциальности
        </Link>
        <Link to="/contact">
          Контакты
        </Link>
        <Link to="/search">
          Поиск
        </Link>
        <Link to="/team">
          Коллектив
        </Link>
        <Link to="/jobs">
          Вакансии
        </Link>
        <Link to="/job">
          Вакансия
        </Link>
      </div>

      <button className="button button__red">
        button button__red
      </button>

      <button className="button button__white">
        "button button__white
      </button>

      <button className="button button--page__error">
        button--page__error
      </button>

      <button className="button button__blue">
        button__blue
      </button>

      <div className="row">
        <div className="tags d-flex flex-wrap mt-3">
          <button className="tags__tag active">
            <span className="tag__text">
              Административный персонал
            </span>
          </button>
          <button className="tags__tag">
            <span className="tag__text">
              Продажи
            </span>
          </button>
        </div>
        <div className="col-4">
          <div className="card-job">

            <div className="card-job__header d-flex justify-content-between align-items-center">
              <div className="card-job__header__map d-flex align-items-center">
                <img className="card-job__header__img" src={Point} alt=""/>
                <span className="card-job__header__text">Орел</span>
              </div>
              <div className="card-job__header--status status status_success">
                Полный день
              </div>
            </div>
            <div className="card-job__job">
              Менеджер по закупкам.
              Менеджер по закупкам.
            </div>

            <div className="card-job__salary">
              <span className="from">от</span>30 000 <span className="rub">₽</span>
            </div>
            <div className="card-job__date">
              18 августа
            </div>
            <div className="card-job__buttons d-flex justify-content-between">
              <button className="button card-job__blue button__blue">Подробнее</button>
              <button className="button card-job__transparent button__transparent">Отправить резюме</button>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-job">

            <div className="card-job__header d-flex justify-content-between align-items-center">
              <div className="card-job__header__map d-flex align-items-center">
                <img className="card-job__header__img" src={Point} alt=""/>
                <span className="card-job__header__text">Орел</span>
              </div>
              <div className="card-job__header--status status status_warning">
                Сменный график
              </div>
            </div>
            <div className="card-job__job">
              Менеджер по закупкам.
              Менеджер по закупкам.
            </div>

            <div className="card-job__salary">
              <span className="from">от</span>30 000 <span className="rub">₽</span>
            </div>
            <div className="card-job__date">
              18 августа
            </div>
            <div className="card-job__buttons d-flex justify-content-between">
              <button className="button card-job__blue button__blue">Подробнее</button>
              <button className="button card-job__transparent button__transparent">Отправить резюме</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Elements;
