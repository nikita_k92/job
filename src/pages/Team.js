import React from 'react';
import Life from "../component/Carousels/Life/Life"
import Slide1 from "../assets/images/slide1.png";
import Photo1 from "../assets/images/photo1.jpg";
import Photo2 from "../assets/images/photo2.jpg";
import Photo3 from "../assets/images/photo3.jpg";
import Photo4 from "../assets/images/photo4.jpg";
import ReviewsCarousel from "../component/Carousels/Reviews/ReviewsCarousel";

const Team = () => {

  return (
    <div className="page">
      <section className="page__team">
        <div className="banner d-flex align-items-center" style={{minHeight: '32.1875rem'}}>
          <img className="banner__image" src={Slide1} alt=""/>
          <div className="container">
            <div className="banner__info">
              <div className="banner__title text-black">
                О коллективе
              </div>
              <div className="banner__text text-black">
                На сегодняшний день у нас работает Около 250 сотрудников в 30 подразделениях
                по всей стране. Наш опыт работы более
                12 лет.
              </div>
            </div>
          </div>
        </div>
        <div className="container">

          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--md title-block--page__team ">
                <h2 className="title-block__text">
                  <b>Руководство компании</b>
                </h2>
              </div>
            </div>
            <div className="col-lg-11 col-12">
              <div className="team">

                <div className="team__main team-main d-flex justify-content-between flex-lg-row flex-column align-items-lg-start align-items-center">
                  <div className="team-main__image">
                    <img src={Photo1} alt=""/>
                  </div>
                  <div className="team-main__info">
                    <div className="team-main__text">
                      Трудовые ресурсы – это самое ценное из всего, чем располагает наша компания. Все мы растем,
                      совершенствуемся и достигаем новых высот. И успех каждого из нас приводит к росту и процветанию
                      всей
                      компании. Каждый новый сотрудник вносит в бизнес свою лепту, каждый приносит в компанию частичку
                      себя и обогащает ее.
                    </div>
                    <div className="team-main__author d-flex justify-content-end">
                      <div className="team-main__block d-inline-block">
                        <span className="team-main__name  d-inline-block">Валухов Роман Владимирович</span>
                        <span className="team-main__post  d-inline-block">генеральный директор</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            <div className="col-lg-10 offset-lg-1 col-md-12 offset-0">
              <div className="team__other team-other d-flex justify-content-between flex-wrap flex-md-row flex-column align-items-md-start align-items-center">

                <div className="team-other__item d-flex flex-column align-items-center">
                  <div className="team-other__image">
                    <img src={Photo2} alt=""/>
                  </div>
                  <div className="team-other__name">
                    Тулинов Иван Александрович
                  </div>
                  <div className="team-other__post">
                    Директор по продажам
                  </div>
                </div>

                <div className="team-other__item d-flex flex-column align-items-center">
                  <div className="team-other__image">
                    <img src={Photo3} alt=""/>
                  </div>
                  <div className="team-other__name">
                    Осюшкин Александр  Сергеевич
                  </div>
                  <div className="team-other__post">
                    Коммерческий директор
                  </div>
                </div>

                <div className="team-other__item d-flex flex-column align-items-center">
                  <div className="team-other__image">
                    <img src={Photo4} alt=""/>
                  </div>
                  <div className="team-other__name">
                    Журавлев Александр Юрьевич
                  </div>
                  <div className="team-other__post">
                    Директор по развитию
                  </div>
                </div>

              </div>
            </div>

          </div>

        </div>

        <div className="container d-flex justify-content-center">
          <div className="line-red"/>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--page__team title-block--md ">
                <h2 className="title-block__text">
                  <b>Истории в лицах</b>
                </h2>
              </div>
              <ReviewsCarousel/>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--page__team title-block--md mb-25">
                <h2 className="title-block__text">
                  <b>Наша жизнь</b>
                </h2>
              </div>
              <Life/>
            </div>
          </div>
        </div>

      </section>
    </div>
  );
};

export default Team;
