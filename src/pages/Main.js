import React, {useContext, useEffect} from 'react'
import A1 from "../assets/images/a1.jpg"
import A2 from "../assets/images/a2.jpg"
import A6 from "../assets/images/a6.jpg"
import FormVector from "../assets/images/form-vector.png"
import Face from "../assets/images/face.jpg"
import Slide from "../assets/images/slide.jpg"
import Form from "../component/Form/Form"
import Brand from "../component/Carousels/Brand/Brand"
import CardJob from "../component/Card-job/Card-job"
import Search from "../component/Search/Search"
import {Link} from "react-router-dom"
import api from "../api/api"
import {MainContext} from "../context/main"
import {NormalizeCategoriesView} from '../utils/Helpers'
import Skeleton, {SkeletonTheme} from "react-loading-skeleton"
import 'react-loading-skeleton/dist/skeleton.css'
import {InterviewContext} from "../context/interview"
import FormSuccess from "../component/FormSuccess/FormSuccess"

const Main = () => {

  const {
    categories, setCategories, loadingCategory, setLoadingCategory,
  } = useContext(MainContext)

  const {
    success, setSuccess
  } = useContext(InterviewContext)

  useEffect(() => {
    document.title = "Главная"
  }, [])

  const list = [
    {
      title: 'Обучение и развитие',
      text: `<p>
В МТК Росберг мы не только помогаем новым сотрудникам адаптироваться к работе, но и систематически проводим тренинги и вебинары.
</p> `,
      img: A1
    },
    {
      title: 'Современный офис',
      text: `<p>В 2020 году состоялось торжественное открытие нового здания MTK Росберг. В отделке интерьера использовались современные дизайнерские решения.</p> `,
      img: A2
    },
    {
      title: 'Бонусы',
      text: `<p>Действует Корпоративное предложение для сотрудников. Например:</p>
              <p class="line">своя столовая;</p>
              <p class="line">скидки на продукцию;</p>
              <p class="line">скидки в фитнес-клуб.</p>`,
      img: A6
    }
  ]

  useEffect(() => {
    api.getFields().then(resp => {
      setCategories(NormalizeCategoriesView(resp.data))
    }).catch(error => {
      console.log('resp', error)
    })
  }, [])

  useEffect(() => {
    if (!categories) return
    setLoadingCategory(false)
  }, [categories])

  return (
    <div className="page">

      <section className="page__main mt-0">

        <div className="banner d-flex align-items-center">
          <img className="banner__image" src={Slide} alt=""/>
          <div className="container">
            <div className="banner__info">
              <div className="banner__title">
                Приглашаем на работу
                в МТК Росберг!
              </div>
              <div className="banner__text">
                На сегодняшний день у нас работает около 250
                сотрудников в 30 подразделениях по всей стране.
                Наш опыт работы более 12 лет.
              </div>
            </div>
            <div className="banner__search">
              <Search modification={"search-big"}/>
            </div>

          </div>

        </div>

        <div className="container">
          <div className="row">
            <div className="col-12">
              {loadingCategory
                ?
                <div className="tags page__main-tags">
                  <SkeletonTheme baseColor="#DCDCDC" highlightColor="#fff">
                    <Skeleton style={{height: '50px'}} count={1}/>
                  </SkeletonTheme>
                </div>
                :
                <div className="tags page__main-tags">
                  {categories.map((item, index) => {
                    if (item.type === 'SECTION') {
                      return item.items.map((it, ind) => {
                        return (
                          <Link to={`/vacancies?SECTION_ID[]=${it.id}`}
                                className="tags__tag" key={it.id}>
                              <span className="tag__text" style={{color: '#8C97AC'}}>
                                {it.value}
                              </span>
                          </Link>
                        )
                      })
                    }
                  })}
                </div>
              }

              <div className="title-block title-block--md title-block--page__main align-items-baseline">
                <h2 className="title-block__text">
                  <b>Найдите работу своей мечты!</b>
                </h2>
                <Link to="/vacancies" className="title-block__link">Смотреть все вакансии</Link>
              </div>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="card-job-list">
            <div className="row">

              <CardJob/>

            </div>
          </div>
        </div>

        <div className="container d-flex justify-content-center">
          <div className="line-red"/>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--md title-block--page__main align-items-baseline">
                <h2 className="title-block__text">
                  <b>Почему нас выбирают?</b>
                </h2>
                <Link to="/advantages" className="title-block__link">Смотреть все преимущества</Link>
              </div>
            </div>
            <div className="col-lg-7 col-12">
              <div className="advantages-block">
                {list.map((item, index) => {
                  return (
                    <div
                      key={index}
                      className="advantages-block__item d-flex justify-content-between align-items-center flex-lg-row flex-column">
                      <div className="advantages-block__image d-flex align-items-center">
                        <img src={item.img} alt=""/>
                      </div>
                      <div className="advantages-block__info">
                        <div className="advantages-block__title">{item.title}</div>
                        <div className="advantages__col-text advantages-block__text"
                             dangerouslySetInnerHTML={{__html: item.text}}/>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>

            <div className="col-xl-4 offset-xl-1 col-lg-5 offset-lg-0  col-12">
              <div className="advantages-info">
                <div className="advantages-info__image">
                  <img src={Face} alt=""/>
                </div>
                <div className="advantages-info__title">
                  Мы ценим ваше время
                </div>
                <div className="advantages-info__text">
                  В МТК Росберг разработан четкий и понятный принцип взаимодействия всех подразделений компании. Но,
                  вместе с тем, мы всегда в поиске новых методов упрощения и автоматизации как внутренних процессов, так
                  и внешних.
                </div>
                <div className="advantages-info__author">
                  Юлия, начальник отдела кадров
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <div className="container">
            <hr className="line"/>
            <div className="row">
              <div className="col-12">
                <div className="title-block title-block--md title-block--page__main">
                  <h2 className="title-block__text">
                    <b>Наши партнеры</b>
                  </h2>
                </div>
              </div>
            </div>
          </div>
          <Brand/>
        </div>

        <div className="container">
          <hr className="line"/>
          {success
            ?
            <div className="mb-30 mt-30">
              <div className="row ">
                <FormSuccess/>
              </div>
            </div>
            :
            <div className="row">
              <div className="col-12">
                <div className="title-block title-block--md title-block--page__main flex-column align-items-start">
                  <h2 className="title-block__text">
                    <b>Заполните анкету</b>
                  </h2>
                  <span className="sub-title">И мы рассмотрим ее в ближайшее время!</span>
                </div>
              </div>
              <div className="col-12 position-relative">
                <Form type="form-main"/>
                <img src={FormVector} className="form-vector" alt=""/>
              </div>
            </div>
          }


        </div>
      </section>
    </div>
  )
}

export default Main
