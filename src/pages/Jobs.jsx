import React, {useContext, useEffect, useState, useRef} from 'react'
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import {Link, useHistory, useLocation} from "react-router-dom"
import {TypeJob} from "../utils/Constants"
import api from "../api/api"
import Point from "../assets/images/point.svg"
import Search from "../assets/images/search.png"
import {MainContext} from "../context/main"
import {NormalizeCategoriesView, NormalizePaginationView, NormalizeVacancyView, Declination} from "../utils/Helpers"
import Filter from "../component/Filter/Filter"
import Tags from "../component/Tags/Tags"
import moment from "moment"
import "moment/locale/ru"
import SearchIcon from "../assets/images/search-null.png"
import {ArrayParam, StringParam, useQueryParam} from 'use-query-params'


const Jobs = () => {

  const {
    setFormModal,
    vacancies, loadingVacancy,
    setLoadingCategory,
    categories, setCategories,
    setLoadingVacancy, createLinkWithParams,
    pagination, setPagination,
    setVacancies,
    checkCategory,
  } = useContext(MainContext)

  let history = useHistory()
  let location = useLocation()

  // http://localhost:3000/vacancies?CITY=255&CHART=&SECTION_ID=
  // http://localhost:3000/vacancies?CITY[]=255&CITY[]=253&SECTION_ID[]=350&CHART[]=247

  const [searchField, setSearchField] = useState('')
  const [CITY, setCITY] = useQueryParam('CITY[]', ArrayParam)
  const [SECTION, setSECTION] = useQueryParam('SECTION_ID[]', ArrayParam)
  const [CHART, setCHART] = useQueryParam('CHART[]', ArrayParam)
  const [FIND, setFIND] = useQueryParam('FIND', StringParam)
  const [PAGE, setPAGE] = useQueryParam('PAGE', StringParam)
  const [COUNT, setCOUNT] = useQueryParam('COUNT', StringParam)
  const [DECORATION, setDECORATION] = useQueryParam('DECORATION[]', ArrayParam)

  const Submit = (e) => {
    e.preventDefault()
    e.stopPropagation()
    checkCategory()
    setLoadingVacancy(true)
    history.push(`${location.pathname}?PAGE=1&COUNT=1&FIND=${encodeURI(searchField)}`)
    api.getVacancyName(searchField).then(resp => {
      setVacancies(NormalizeVacancyView(resp.data.ITEMS))
      setPagination(NormalizePaginationView(resp.data))
    }).catch(error => {
      console.log('resp', error)
    })
  }

  let link = createLinkWithParams({CITY, SECTION, CHART, FIND, PAGE, COUNT, DECORATION})

  useEffect(() => {
    document.title = "Вакансии"
    if (link.length > 0) { //если есть параметры в ссылке

      setSearchField(FIND ? decodeURI(FIND) : '')
      api.getVacancyFilter(link).then(resp => {
        setVacancies(NormalizeVacancyView(resp.data.ITEMS))
        setPagination(NormalizePaginationView(resp.data))
      }).catch(error => {
        console.log('resp', error)
      })
      api.getFields().then(resp => {
        setCategories(NormalizeCategoriesView(resp.data, {CITY, SECTION, CHART, FIND, PAGE, COUNT, DECORATION}))
      }).catch(error => {
        console.log('resp', error)
      })
    } else {
      api.getAllVacancies().then(resp => {
        setVacancies(NormalizeVacancyView(resp.data.ITEMS))
        setPagination(NormalizePaginationView(resp.data))
      }).catch(error => {
        console.log('resp', error)
      })
      api.getFields().then(resp => {
        setCategories(NormalizeCategoriesView(resp.data))
      }).catch(error => {
        console.log('resp', error)
      })
    }
  }, [location.search])



  useEffect(() => {
    if (!vacancies) return
    setLoadingVacancy(false)
  }, [vacancies])

  useEffect(() => {
    if (!categories) return
    createLinkWithParams({CITY, SECTION, CHART})
    setLoadingCategory(false)
  }, [categories !== null])

  const paginationElement = () => {
    const pageNumber = []
    for (let i = 1; i <= Math.ceil(pagination.SELECTED_COUNT / pagination.COUNT); i++) {
      pageNumber.push(i)
    }

    const goToNaxyi = (number) => {
      history.push(`${location.pathname}?${createLinkWithParams({CITY, SECTION, CHART, DECORATION}, number)}`)
    }

    const renderPageNumbers = pageNumber.map(number => {
      return (
        <div
          onClick={e => goToNaxyi(number)}
          key={number}
          className={"pagination__num " + (number === pagination.PAGE ? 'pagination__num--active' : '')}>
          {number}
        </div>
      )
    })
    return (
      <div className="pagination__pagination ml-auto justify-content-end d-flex mt-5 mb-5">

        {pagination.PAGE > 1
        &&
        <div onClick={e => goToNaxyi(pagination.PAGE - 1)}
             className="button button__gray pagination__button__gray">Назад</div>
        }
        {renderPageNumbers}
        {pagination.SELECTED_COUNT > (pagination.COUNT * pagination.PAGE)
        &&
        <div onClick={e => goToNaxyi(pagination.PAGE + 1)}
             className="button button__gray pagination__button__gray ml-5">Дальше</div>
        }

      </div>
    )
  }

  return (
    <div className="page">
      <section className="page__jobs page-jobs">
        <div className="container">
          <div className="title-block title-block--md title-block--page__jobs">
            <h2 className="title-block__text">
              <b>Вакансии</b>
            </h2>
          </div>
          <div className="page-jobs__search" onSubmit={Submit}>
            <form className="search d-flex">
              <div className="search__form search--border d-flex w-100">
                <div className="search__input border-0 d-flex align-items-center">
                  <img className="search__icon" src={Search} alt="Поиск"/>
                  <input
                    onChange={e => setSearchField(e.target.value)}
                    value={searchField || ''}
                    type="text"
                    className="form-control"
                    placeholder="Введите желаемую должность"/>
                </div>
              </div>
              {/*<button  className="button button__red search__button">Найти вакансию</button>*/}
            </form>
          </div>

          <div className="row mb-30">
            <div className="col-lg-3  col-md-12" >

              <Filter/>

            </div>

            <div className="col-lg-9 col-md-12">
              {loadingVacancy
                ?
                <SkeletonTheme baseColor="#f3f3f3" highlightColor="#ecebeb">
                  <Skeleton className="vacancy-skeleton mt-10 mb-10" count={1}/>
                </SkeletonTheme>
                :
                <>
                  {vacancies?.length > 0
                    ?
                    <>
                      <div className="jobs-info-search d-flex justify-content-center">
                        <div className="jobs-info-search__count">Найдено {pagination?.SELECTED_COUNT} &nbsp;
                        {Declination(pagination?.SELECTED_COUNT, ['вакансия', 'вакансии', 'вакансий'])} </div>
                      </div>

                      <div className="card-job">

                        <Tags/>

                        {vacancies?.length > 0
                          ?
                          vacancies.map((item, index) => {
                            return (
                              <div className="card-job--big"
                                   key={item.ID}>
                                <div
                                  className="card-job__job  mb-10 minh-auto d-flex align-items-center justify-content-between flex-wrap">
                                  <Link to={"/vacancy/" + item.ID} target="_blank" className="card-job__job--color">
                                    {item.NAME}
                                  </Link>
                                  <div
                                    className={"card-job__header--status status  mt-5 mb-5 status_" + (TypeJob[item.CHART.ENUM_ID].class)}>
                                    {TypeJob[item.CHART.ENUM_ID].text}
                                  </div>
                                </div>

                                <div className="d-flex align-items-center justify-content-between mb-15 flex-wrap">
                                  <div className="card-job__header__map d-flex align-items-center ">
                                    <img className="card-job__header__img" src={Point} alt=""/>
                                    <span className="card-job__header__text">{item.CITY.VALUE}</span>
                                  </div>
                                  <div className="card-job__date">
                                    {/*{item.ACTIVE_FROM.split(' ')[0]}*/}
                                    {moment(item.ACTIVE_FROM.split(' ')[0], 'DD.MM.YYYY').format('LL')}
                                  </div>
                                </div>

                                <p className="card-job__desc" dangerouslySetInnerHTML={{__html:  item?.PREVIEW_TEXT}}/>

                                <div
                                  className="card-job__buttons d-flex justify-content-start flex-wrap align-items-center">
                                  <Link to={"/vacancy/" + item.ID}
                                        target="_blank"
                                        className="button card-job__blue button__blue mr-0 mt-0">
                                    Подробнее
                                  </Link>
                                  <button
                                    onClick={e=> setFormModal(true)}
                                    className="button card-job__transparent button__transparent ml-25 mr-0 mt-0">
                                    Отправить резюме
                                  </button>
                                  <div className="card-job__salary ml-auto mt-10 mb-10">
                                    {/*<span className="from">от</span>130 000 <span className="rub">₽</span>*/}
                                    {item.PAY}
                                    {/*<span className="rub">₽</span>*/}
                                  </div>
                                </div>
                                <hr className="mb-10 mt-10"/>
                              </div>
                            )
                          })
                          :
                          ''
                        }
                        {pagination?.COUNT < pagination?.SELECTED_COUNT
                        &&
                        <div className="pagination d-flex align-items-center justify-content-between flex-wrap">
                          {/*<div className="pagination__link-page d-flex align-items-center mt-5 mb-5">*/}
                          {/*<div className="pagination__link-text mr-10">Перейти на страницу</div>*/}
                          {/*<form className="pagination__form form-group mb-0 mr-10">*/}
                          {/*<input type="text" className="form-control mb-0 p-0 text-center"/>*/}
                          {/*</form>*/}
                          {/*<a href="#" className="button button__gray disabled pagination__button__gray mr-10">Перейти</a>*/}
                          {/*</div>*/}
                          {paginationElement()}
                        </div>
                        }

                      </div>
                    </>
                    :
                    <div className="col-12">
                      <div className="title-block title-block--md title-block--page__contact">
                        <h2 className="title-block__text">
                          <b>Результат поиска не найден</b>
                        </h2>
                      </div>
                      <div className="search-null">
                        Попробуйте поискать другую вакансию.
                      </div>
                      <div className="search-result">
                        <div className="search-stub d-flex align-items-center justify-content-center">
                          <img className="search-stub__image" src={SearchIcon} alt=""/>
                        </div>
                      </div>
                    </div>
                  }
                </>
              }
            </div>


          </div>
        </div>

      </section>
    </div>
  )
}

export default Jobs
