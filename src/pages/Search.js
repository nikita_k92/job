import React from 'react';
import SearchIcon from "../assets/images/search-null.png"
import Search from "../component/Search/Search";


const PrivacyPolicy = () => {

  return (
    <div className="page">
      <section className="page__search">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--md title-block--page__contact">
                <h2 className="title-block__text">
                  <b>Результат поиска не найден</b>
                </h2>
              </div>
              <div className="search-null">
                Попробуйте поискать другую вакансию.
              </div>
              <Search/>

              <div className="search-result">
                <div className="search-stub d-flex align-items-center justify-content-center">
                  <img className="search-stub__image" src={SearchIcon} alt=""/>
                </div>

              </div>

            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default PrivacyPolicy;
