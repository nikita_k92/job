import React, {useContext, useState} from 'react'
import Phone from "../assets/images/phone-icon.svg"
import Mail from "../assets/images/mail-icon.svg"
import MapPoint from "../assets/images/map-point.png"
import {Map, Placemark, YMaps} from "react-yandex-maps"
import {Constants} from "../utils/Constants"
import Check from "../assets/images/check.svg"
import InputMask from 'react-input-mask'
import {InterviewContext} from "../context/interview"
import api from "../api/api"
import Loading from "../component/Loading/Loading"
import FormSuccess from "../component/FormSuccess/FormSuccess"
import myIcon from "../assets/images/map-point.png";

const PrivacyPolicy = () => {


  const mapState = {center: [53.014766, 36.156452], zoom: 14};

  const placeMark = {
    geometry: [53.014766, 36.156452],
    properties: {
      hintContent: '',
      balloonContent: 'г.Орел, ул. Московское шоссе д. 173'
    },
    options:{
      iconLayout: 'default#image',
      iconImageHref: myIcon,
      iconImageSize: [40, 52],
      iconImageOffset: [-20, -52],
    },
    modules: ['geoObject.addon.balloon', 'geoObject.addon.hint']
  }

  const mapData = {
    center: [53.014766, 36.156452],
    zoom: 14,
  }

  const {
    phone, setPhone, agreement, setAgreement, success, setSuccess,
    loadingSuccess, setLoadingSuccess
  } = useContext(InterviewContext)

  const [phoneError, setPhoneError] = useState('')

  const handleCheckAgreement = (e, agr) => {
    e.preventDefault()
    e.stopPropagation()
    setAgreement(!agr)
  }

  const submitForm = (e) => {
    e.preventDefault()
    e.stopPropagation()
    if (!agreement) return
    setLoadingSuccess(true)
    setPhoneError('')
    const formData = new FormData()
    formData.append('PHONE', phone)
    formData.append('ACTION', 'CALLBACK')
    api.getInterview(formData).then(resp => {
      setPhone('')
      setSuccess(true)
      setLoadingSuccess(false)

    }).catch(e => {
      console.log(e.response.data.ERROR)
      setPhoneError(e.response.data.ERROR.phone)
      setLoadingSuccess(false)
    })
  }

  return (
    <div className="page">
      <section className="page__contact">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--md title-block--page__contact">
                <h2 className="title-block__text">
                  <b>Контакты</b>
                </h2>
              </div>
              <div className="name-organization mb-20">
                ООО “МТК Росберг Центр”
              </div>
              <hr/>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-12">
              <div className="contact-block">
                <div className="contact-block__text">
                  Записаться на собеседование вы можете по телефонам и с помощью почты:
                </div>
                <div className="contact-block__contacts contact-contacts">
                  <div className="contact-contacts__item  d-flex align-items-center">
                    <div className="contact-contacts__img">
                      <img src={Phone} alt="Телефон"/>
                    </div>
                    <a href={`tel:${Constants.phone_format}`} className="contact-contacts__text">
                      {Constants.phone_text}
                    </a>
                  </div>
                  <div className="contact-contacts__item d-flex align-items-center">
                    <div className="contact-contacts__img">
                      <img src={Phone} alt="Телефон"/>
                    </div>
                    <a href={`tel:${Constants.phone_2_format}`} className="contact-contacts__text ">
                      {Constants.phone_2_text}
                    </a>
                  </div>
                  <div className="contact-contacts__item  d-flex align-items-center">
                    <div className="contact-contacts__img">
                      <img src={Mail} alt="Почта"/>
                    </div>
                    <a href={`mailto:${Constants.email}`} className="contact-contacts__text">
                      {Constants.email}
                    </a>
                  </div>
                </div>
                <div className="contact-block__info">
                  <p style={{color: '#333333'}}>Дубцова Юлия Романовна</p>
                  <p>начальник отдела кадров</p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-12">
              {success
                ?
                <div className="contact-block">
                  <FormSuccess/>
                </div>
                :
                <div className="contact-block">
                  <div className="contact-block__text">
                    Вы можете указать свой номер телефона, и менеджер свяжется с вами:
                  </div>

                  <form className="contact-block__form form-group" onSubmit={submitForm}>
                    <InputMask
                      // required={true}
                      className="form-control"
                      onChange={(e) => setPhone(e.target.value)}
                      value={phone || ''}
                      mask="+7(999) 999 99 99" maskChar={null}
                    />
                    <div
                      className={"form__error mb-10 " + (phoneError ? 'form__error-visible ' : '')}>{phoneError}</div>
                    {/*<input type="text" className="form-control" placeholder="+7("/>*/}
                    <div className="d-flex justify-content-between flex-column">
                      {!loadingSuccess
                        ?
                        <button
                          type="submit"
                          className={"button button__red contact-block__button-red " + (!agreement ? 'button__red--no-click' : '')}>
                          Отправить
                        </button>
                        :
                        <div className="loading form__loading">
                          <Loading/>
                        </div>
                      }


                    </div>

                  </form>
                  {!loadingSuccess
                  &&
                  <div className="contact-block__info form__info">
                    <div
                      onClick={(e) => handleCheckAgreement(e, agreement)}
                      className="checkbox-fade fade-in-primary checkbox-fade--mb-10 ">
                      <label className="check-task align-items-center">
                        <input type="checkbox" checked={agreement} readOnly/>

                        <span className="cr ml-5 mr-5 order-2">
                          <img src={Check} alt=""/>
                        </span>
                        <span className="order-1">
                        Я согласен с условиями передачи данных.
                        <a href="" className="d-inline-block  order-1">Соглашение</a>.
                        </span>
                      </label>
                    </div>
                  </div>
                  }

                </div>
              }

            </div>
            <div className="col-lg-4 col-12">
              <div className="contact-block">
                <div className="contact-block__text">
                  Наш адрес:
                </div>
                <div className="contact-block__chart">
                  <p>г.Орел, ул. Московское шоссе д. 173 </p>
                  <p><span>пн-пт</span> с 9:00 до 17:00</p>
                  <p><span>сб</span> с 10:00 до 14:00</p>
                </div>
              </div>
            </div>
          </div>
          <hr className="line mb-30" style={{marginTop: "50px"}}/>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="map">
                <YMaps>
                  <Map state={mapState} width="100%"
                       height="20.625rem">
                    <Placemark {...placeMark} />
                  </Map>
                </YMaps>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default PrivacyPolicy
