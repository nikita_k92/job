import React from 'react';
import A1 from "../assets/images/a1.jpg"
import A2 from "../assets/images/a2.jpg"
import A3 from "../assets/images/a3.jpg"
import A4 from "../assets/images/a4.jpg"
import A5 from "../assets/images/a5.jpg"
import A6 from "../assets/images/a6.jpg"
import A7 from "../assets/images/a7.jpg"
import A8 from "../assets/images/a8.jpg"
import A9 from "../assets/images/a9.jpg"

const Advantages = () => {

  const list_ad = [
    {
      title: 'Обучение и развитие',
      text: `<p>
В МТК Росберг мы не только помогаем новым сотрудникам адаптироваться к работе, но и систематически
                      проводим тренинги и вебинары как с ведущими специалистами компании, так и с приглашенными
                      спикерами.
</p> `,
      img: A1
    },
    {
      title: 'Современный офис',
      text: `<p>В 2020 году состоялось торжественное открытие
             нового здания MTK Росберг. В отделке интерьера использовались современные дизайнерские
              решения, основанные на применении натуральных отделочных материалов.</p> `,
      img: A2
    },
    {
      title: 'Премии',
      text: `<p class="line">Профессиональный, карьерный и финансовый рост;</p>
              <p class="line">Премирование лучших сотрудников ежеквартально;</p>
              <p class="line">Инициативные сотрудники получают бонусы.</p>`,
      img: A3
    },
    {
      title: 'Компенсация затрат',
      text: `<p>Для вас как для сотрудника копании предусмотрен ряд компенсационных выплат, а именно:</p>
              <p class="line">Премирование лучших сотрудников ежеквартально;</p>
              <p class="line">Инициативные сотрудники получают бонусы.</p>`,
      img: A4
    },
    {
      title: 'Корпоративный транспорт ',
      text: `<p>В МТК Росберг мы не только помогаем новым сотрудникам адаптироваться к работе, но и  
систематически проводим тренинги и вебинары как с ведущими специалистами компании, так  и с приглашенными спикерами.</p>`,
      img: A5
    },
    {
      title: 'Бонусы',
      text: `<p>Корпоративное предложение для сотрудников:</p>
              <p class="line">своя столовая;</p>
              <p class="line">корпоративные скидки в фитнес-клуб;</p>
              <p class="line">спокойная рабочая атмосфера;</p>
              <p class="line">скидки на продукцию.</p>`,
      img: A6
    },
    {
      title: 'Корпоративные мероприятия',
      text: `<p>Вы станете участником корпоративных мероприятий, которые мы организуем для поднятия настроения сотрудников:</p>
              <p class="line">спортивные и культурные события;</p>
              <p class="line">праздники компании;</p>
              <p class="line">конкурсы и многие другие.</p>`,
      img: A7
    },
    {
      title: 'Самореализация',
      text: `<p>Сотрудники участвуют в разработке и реализации крупных проектов компании и, 
также, активно вносят предложения по улучшениям корпоративной жизни, взаимодействию с партнерами и клиентами.</p>`,
      img: A8
    },
    {
      title: 'Выставки и форумы',
      text: `<p>
 В МТК Росберг мы не только помогаем новым сотрудникам адаптироваться к работе, но и систематически
  проводим тренинги и вебинары как с ведущими специалистами компании, так  и с приглашенными спикерами.</p>`,
      img: A9
    }
  ]
  // <p><span></span>Премирование лучших сотрудников ежеквартально;<br/></p>
  //           <p><span></span>Инициативные сотрудники получают бонусы.</p>\`,
  return (
    <div className="page">
      <section className="page__advantages">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="title-block title-block--md title-block--page__advantages ">
                <h2 className="title-block__text">
                  <b>Преимущества</b>
                </h2>
              </div>
              {list_ad.map((item, index) => {
                return (
                  <div key={index} className="advantages">
                    <div className={"row d-flex justify-content-between flex-lg-row flex-column align-items-center" + (index % 2 !== 0 ? ' flex-lg-row-reverse' : ' ' )}>
                      <div className="advantages__col col-lg-6 col-12 d-flex  justify-content-center  flex-column">
                        <h6 className="advantages__col-title">
                          {item.title}
                        </h6>
                        <div className="advantages__col-text"
                             dangerouslySetInnerHTML={{__html: item.text}}>
                        </div>
                      </div>
                      <div className="col-lg-6 col-12 d-flex">
                        <div className="advantages__col-img">
                          <img src={item.img} alt=""/>
                        </div>

                      </div>
                    </div>
                  </div>
                )
              })}


            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Advantages;
