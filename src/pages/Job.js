import React, {useContext, useEffect, useState} from 'react'
import "../component/Carousels/Jobs/style.sass"
import Slider from "react-slick"
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import {Link, useParams} from "react-router-dom"
import Point from "../assets/images/point.svg"
import Avito from "../assets/images/avito.png"
import HH from "../assets/images/hh.png"
import SJ from "../assets/images/superjob.png"
import Arrow from "../assets/images/arrow.svg"
import {MainContext} from "../context/main"
import api from "../api/api"
import {NormalizeVacancyView, price} from "../utils/Helpers"
import moment from "moment"
import {Constants, TypeJob} from "../utils/Constants"
import Phone from "../assets/images/phone-icon.svg"
import Mail from "../assets/images/mail-icon.svg"


const Job = () => {
  const params = useParams()
  const {vacancies, formModal, setFormModal, setVacancy, vacancy, setLoadingVacancy, setVacancies} = useContext(MainContext)

  const [loading, setLoading] = useState(true)
  const [showContact, setShowContact] = useState(false)

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    className: 'jobs-carousel carousel-custom-arrow',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  }

  const fetchData = () => {
    setLoading(true)
    api.getVacancy(params.id).then(resp => {
      let v
      Object.keys(resp.data.ITEMS).forEach(key => v = resp.data.ITEMS[key])
      setVacancy(v)
      setLoading(false)
    }).catch(error => {
      console.log('resp', error)
    })
    api.getAllVacancies().then(resp => {
      setVacancies(NormalizeVacancyView(resp.data.ITEMS))
    }).catch(error => {
      console.log('resp', error)
    })
  }

  useEffect(() => {
    fetchData()
  }, [params])

  useEffect(() => {
    document.title = "Вакансия"
    fetchData()
    return () => {
      setLoading(true)
      setVacancy(null)
      console.log('exit')
    }
  }, [])

  useEffect(() => {
    if (!vacancy) return
    setLoading(false)
  }, [vacancy])

  useEffect(() => {
    if (!vacancy) return
    setLoadingVacancy(false)
  }, [vacancies])

  let list = vacancies?.map((item, index) => {
    return (
      <div className="card-job" key={item.ID}>
        <div className="card-job__header d-flex justify-content-between align-items-center">
          <div className="card-job__header__map d-flex align-items-center">
            <img className="card-job__header__img" src={Point} alt=""/>
            <span className="card-job__header__text">{item.CITY.VALUE}</span>
          </div>
          <div className={"card-job__header--status status status_" + (TypeJob[item.CHART.ENUM_ID].class)}>
            {TypeJob[item.CHART.ENUM_ID].text}
          </div>
        </div>
        <div className="card-job__job">
          <Link to={"/vacancy/" + item.ID} style={{color: '#38484e'}}>
            {item.NAME}
          </Link>
        </div>
        <div className="card-job__salary">
          {price(item.PAY)} <span className="rub">₽</span>
          {/*<span className="from">от</span>30 000 <span className="rub">₽</span>*/}
        </div>
        <div className="card-job__date">
          {moment(item.ACTIVE_FROM.split(' ')[0], 'DD.MM.YYYY').format('LL')}
        </div>
        <div className="card-job__buttons d-flex justify-content-between">
          <Link to={"/vacancy/" + item.ID}
                className="button card-job__blue button__blue">Подробнее
          </Link>
          <button className="button card-job__transparent button__transparent">Отправить резюме</button>
        </div>
      </div>
    )
  })

  return (
    <div className="page">
      <section className="page__vacancy page-vacancy">
        <div className="container">
          <Link to="/vacancies" className="back-jobs mb-10">
            <img src={Arrow} className="back-jobs__img" alt="назад"/>
            <span>К списку вакансий</span>
          </Link>
          <div className="title-block title-block--md title-block--page__job">
            <h2 className="title-block__text">
              <b>Вакансии</b>
            </h2>
          </div>
          {loading ?
            <SkeletonTheme baseColor="#f3f3f3" highlightColor="#ecebeb">
              <Skeleton className="vacancy-skeleton" count={1}/>
            </SkeletonTheme>
            :
            <div className="vacancy">
              <div className="vacancy__title mb-10">
                <h1>
                  <span>
                   {vacancy?.NAME}
                  </span>
                </h1>
              </div>
              <div className="vacancy__salary">
            <span>
              {vacancy?.PAY} до вычета налогов
            </span>
              </div>
              <div className="vacancy__map mb-15">
                <div className="card-job__header__map d-flex align-items-center ">
                  <img className="card-job__header__img" src={Point} alt=""/>
                  <span className="card-job__header__text">{vacancy?.CITY.VALUE}</span>
                </div>
              </div>
              <div className="vacancy__create-date">
                Вакансия опубликована {moment(vacancy?.ACTIVE_FROM.split(' ')[0], 'DD.MM.YYYY').format('LL')}
              </div>
              <div className="vacancy__desc">
                <p dangerouslySetInnerHTML={{__html:  vacancy?.PREVIEW_TEXT}}/>
                <br/>
                <br/>
                <p><b>Оформление:</b> {vacancy?.DECORATION.VALUE}</p>
                <p><b>График работы:</b> {vacancy?.WORK_SCHEDULE}</p>
                <p><b>Выплаты:</b> {vacancy?.PAYMENTS}</p>
              </div>
              <hr/>
              {/*<div className="vacancy__contact">*/}
              {/*  <b>4564564564</b>*/}
              {/*</div>*/}

              <div className="vacancy__info">
                <div className="row">

                  {vacancy?.RESPONSIBILITIES
                  &&
                  <div className="col-md-6 col-12">
                    <div className="advantages-block__info">
                      <div className="advantages-block__title">Обязанности:</div>
                      <div className="advantages__col-text advantages-block__text">
                        {vacancy.RESPONSIBILITIES.map(item => {
                          return(
                            <p className="line">{item}</p>
                          )
                        })}
                      </div>
                    </div>
                  </div>
                  }

                  {vacancy?.REQUIREMENTS
                  &&
                  <div className="col-md-6 col-12">
                    <div className="advantages-block__info">
                      <div className="advantages-block__title">Требования:</div>
                      <div className="advantages__col-text advantages-block__text">
                        {vacancy.REQUIREMENTS.map(item => {
                          return(
                            <p className="line">{item}</p>
                          )
                        })}
                      </div>
                    </div>
                  </div>
                  }

                </div>
              </div>

              <div className="vacancy__buttons vacancy-buttons d-flex flex-wrap mt-30">
                <button
                  onClick={e => setFormModal(!formModal)}
                  className="button button__blue vacancy-buttons__button__blue mr-20">
                  Записаться на собеседование
                </button>
                <button
                  onClick={e=>setShowContact(!showContact)}
                  className="button button__transparent vacancy-buttons__button__contact mr-20">
                  {!showContact
                    ?
                    'Показать контакты'
                    :
                    'Скрыть контакты'
                  }
                </button>

                {vacancy?.AVITO
                &&
                <a target="_blank" href={vacancy.AVITO}
                   className="button button__transparent vacancy-buttons__link  mr-10">
                  <img src={Avito} alt="Авито"/>
                </a>
                }
                {vacancy?.HEADHUNTER
                &&
                <a target="_blank" href={vacancy.HEADHUNTER}
                   className="button button__transparent vacancy-buttons__link mr-10">
                  <img src={HH} alt="HH"/>
                </a>
                }
                {vacancy?.SUPERJOBS
                &&
                <a target="_blank" href={vacancy.SUPERJOBS}
                   className="button button__transparent vacancy-buttons__link mr-10">
                  <img src={SJ} alt="SuperJob"/>
                </a>
                }
              </div>

              <div className="vacancy__contact " style={showContact ? {display: 'block'} : {display: 'none'}}>
                <div className="advantages-block__title">Контакты:</div>
                  <div className="advantages-block__info">
                    <div className="row">
                    <div className="col-md-3 col-12 mb-30 mr-20" style={{maxWidth: '19.375rem', width: '100%'}}>
                      <div className="contact-block mt-0">
                        <div className="contact-block__chart">
                          <p>г.Орел, ул. Московское шоссе д. 173 </p>
                          <p><span>пн-пт</span> с 9:00 до 17:00</p>
                          <p><span>сб</span> с 10:00 до 14:00</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-12 mb-30">
                      <div className="contact-block__contacts contact-contacts mb-0">
                        <div className="contact-contacts__item  d-flex align-items-center">
                          <div className="contact-contacts__img">
                            <img src={Phone} alt="Телефон"/>
                          </div>
                          <a href={`tel:${Constants.phone_format}`} className="contact-contacts__text">
                            {Constants.phone_text}
                          </a>
                        </div>
                        <div className="contact-contacts__item d-flex align-items-center">
                          <div className="contact-contacts__img">
                            <img src={Phone} alt="Телефон"/>
                          </div>
                          <a href={`tel:${Constants.phone_2_format}`} className="contact-contacts__text ">
                            {Constants.phone_2_text}
                          </a>
                        </div>
                        <div className="contact-contacts__item  d-flex align-items-center">
                          <div className="contact-contacts__img">
                            <img src={Mail} alt="Почта"/>
                          </div>
                          <a href={`mailto:${Constants.email}`} className="contact-contacts__text">
                            {Constants.email}
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


              <hr/>
            </div>
          }
          <div className="title-block title-block--md mb-25">
            <h2 className="title-block__text">
              <b>Другие вакансии</b>
            </h2>
          </div>

          <Slider {...settings}>
            {list}
          </Slider>

        </div>
      </section>
    </div>
  )


}

export default Job
